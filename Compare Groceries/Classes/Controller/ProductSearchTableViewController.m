//
//  ProductSearchTableViewController.m
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "ProductSearchTableViewController.h"
#import "ProductTableViewCell.h"

typedef enum {
    tableViewSearch = 0,
    tableviewNormal = 1
} tableViewType;

@interface ProductSearchTableViewController() <UISearchDisplayDelegate, UISearchBarDelegate>

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) PFQuery *searchQuery;

@end

@implementation ProductSearchTableViewController

- (id)initWithCoder:(NSCoder *)aCoder {
    self = [super initWithCoder:aCoder];
    if (self) {
        // The className to query on
        self.parseClassName = @"AllProducts";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 25;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.tableView.tableHeaderView = self.searchBar;
    
    self.searchController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.searchController.searchResultsDataSource = self;
    self.searchController.searchResultsDelegate = self;
    self.searchController.delegate = self;
    self.searchController.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.searchResults = [NSMutableArray array];
}

- (void)filterResults:(NSString *)searchTerm {
    [self.searchResults removeAllObjects];
    
    if (self.searchQuery) {
        [self.searchQuery cancel];
    }
    
    self.searchQuery = [PFQuery queryWithClassName:self.queryForTable.parseClassName];
    [_searchQuery whereKeyExists:@"canonicalname"];  //this is based on whatever query you are trying to accomplish
    [_searchQuery whereKey:@"canonicalname" containsString:[searchTerm lowercaseString]];
    _searchQuery.limit = 25;
    
    [self.searchQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        [self.searchResults removeAllObjects];
        [self.searchResults addObjectsFromArray:objects];
        [self.searchController.searchResultsTableView reloadData];
        
        self.searchQuery = nil;
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterResults:searchString];
    return YES;
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        return self.objects.count;
    }
    else {
        return self.searchResults.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProductTableViewCell" owner:self options:kNilOptions] objectAtIndex:0];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        object = [self.searchResults objectAtIndex:indexPath.row];
    }

    cell.productName.text = [object objectForKey:@"productname"];
    cell.productSize.text = [[[object objectForKey:@"productsize"] stringByReplacingOccurrencesOfString:@"Units: " withString:@""] uppercaseString];
    cell.colesPrice.text = [object objectForKey:@"colesprice"];
    cell.woolworthsPrice.text = [object objectForKey:@"woolworthsprice"];

    
    CGFloat colesPrice = [[[object objectForKey:@"colesprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
    CGFloat woolworthsPrice = [[[object objectForKey:@"woolworthsprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
    
    if (colesPrice < woolworthsPrice) {
        cell.cardbackground.layer.borderColor = [UIColor colorWithRed:0.9 green:0.5 blue:0.5 alpha:1.0].CGColor;
        cell.cardbackground.layer.borderWidth = 1;
    }
    else {
        cell.cardbackground.layer.borderColor = [UIColor colorWithRed:0.4 green:0.8 blue:0.5 alpha:1.0].CGColor;
        cell.cardbackground.layer.borderWidth = 1;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PFObject *object;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        object = self.searchResults[indexPath.row];
    }
    else {
        object = self.objects[indexPath.row];
    }

    [[DataSingleton sharedSingleton] addItemToShoppingList:object];
    
    [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:[NSString stringWithFormat:@"%ld", [DataSingleton sharedSingleton].colesShoppingList.count + [DataSingleton sharedSingleton].woolworthsShoppingList.count]];
}

@end
