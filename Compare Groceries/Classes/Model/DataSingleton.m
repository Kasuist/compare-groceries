//
//  DataSingleton.m
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "DataSingleton.h"

@implementation DataSingleton

static DataSingleton *_sharedSingleton;

+ (DataSingleton *)sharedSingleton {
    @synchronized([DataSingleton class]) {
        if(!_sharedSingleton)
            _sharedSingleton = [[self alloc] init];
        return _sharedSingleton;
    }
    return nil;
}

+ (id)alloc {
    @synchronized([DataSingleton class]) {
        NSAssert(_sharedSingleton == nil, @"Attempted to allocate a second instance of the Singleton singleton");
        _sharedSingleton = [super alloc];
        return _sharedSingleton;
    }
    return nil;
}

- (instancetype)init {
    if ((self = [super init])) {
        self.colesShoppingList = [[NSMutableArray alloc] init];
        self.woolworthsShoppingList = [[NSMutableArray alloc] init];
        self.combinedShoppingList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addItemToShoppingList:(PFObject *)object {
    [self.combinedShoppingList addObject:object];

    CGFloat colesPrice = [[[object objectForKey:@"colesprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
    CGFloat woolworthsPrice = [[[object objectForKey:@"woolworthsprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
    
    if (colesPrice < woolworthsPrice) {
        if (![self.colesShoppingList containsObject:object]) {
            [self.colesShoppingList addObject:object];
        }
    }
    else {
        if (![self.woolworthsShoppingList containsObject:object]) {
            [self.woolworthsShoppingList addObject:object];
        }
    }
    NSLog(@"Shopping list count: \n Woolworths: %ld \n Coles: %ld", self.woolworthsShoppingList.count, self.colesShoppingList.count);
}

@end