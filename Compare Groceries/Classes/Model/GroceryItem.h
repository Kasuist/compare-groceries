//
//  GroceryItem.h
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GroceryItem : NSObject

@property (nonatomic, readonly) BOOL isAvailableAtColes;
@property (nonatomic, readonly) BOOL isAvailableAtWoolworths;
@property (nonatomic, readonly) CGFloat price;

@end
