//
//  DataSingleton.h
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface DataSingleton : NSObject

@property (strong, nonatomic) NSMutableArray *woolworthsShoppingList;
@property (strong, nonatomic) NSMutableArray *colesShoppingList;
@property (strong, nonatomic) NSMutableArray *combinedShoppingList;

+ (DataSingleton *)sharedSingleton;
- (void)addItemToShoppingList:(PFObject *)object;

@end
