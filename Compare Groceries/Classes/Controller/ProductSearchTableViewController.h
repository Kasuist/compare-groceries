//
//  ProductSearchTableViewController.h
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface ProductSearchTableViewController : PFQueryTableViewController

@end
