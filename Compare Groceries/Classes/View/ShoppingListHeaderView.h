//
//  ShoppingListHeaderView.h
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingListHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *onlyAtColesLabel;
@property (weak, nonatomic) IBOutlet UILabel *onlyAtWoolworthsLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalSavingsLabel;

@end
