//
//  ProductTableViewCell.h
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *productSize;
@property (weak, nonatomic) IBOutlet UILabel *colesPrice;
@property (weak, nonatomic) IBOutlet UILabel *woolworthsPrice;

@property (weak, nonatomic) IBOutlet UIView *cardbackground;

@end
