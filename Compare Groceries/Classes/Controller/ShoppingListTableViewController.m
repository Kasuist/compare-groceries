//
//  ShoppingListTableViewController.m
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "ShoppingListTableViewController.h"
#import "DataSingleton.h"
#import "ProductTableViewCell.h"
#import <Parse/Parse.h>
#import "ShoppingListHeaderView.h"

@implementation ShoppingListTableViewController {
    NSArray *_combinedArray;
    ShoppingListHeaderView *_tableViewHeader;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _combinedArray = @[[DataSingleton sharedSingleton].colesShoppingList, [DataSingleton sharedSingleton].woolworthsShoppingList];
    [self setupTableViewHeader];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableViewHeader];
}

- (void)setupTableViewHeader {
    if (!_tableViewHeader) {
        _tableViewHeader = [[[NSBundle mainBundle] loadNibNamed:@"ShoppingListHeaderView" owner:self options:nil] objectAtIndex:0];
        _tableViewHeader.frame = CGRectMake(0, 0, self.view.frame.size.width, 100);
        self.tableView.tableHeaderView = _tableViewHeader;
    }
    
    CGFloat colesTotal;
    CGFloat woolworthsTotal;
    CGFloat savings;
    for (PFObject *object in [DataSingleton sharedSingleton].combinedShoppingList) {
        CGFloat tempColes = [[[object objectForKey:@"colesprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
        CGFloat tempWoolies = [[[object objectForKey:@"woolworthsprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
        
        if (tempColes < tempWoolies) {
            savings += tempColes;
        }
        else {
            savings += tempWoolies;
        }
        
        colesTotal += tempColes;
        woolworthsTotal += tempWoolies;
    }
    
    _tableViewHeader.onlyAtColesLabel.text = [NSString stringWithFormat:@"$%.2f", colesTotal];
    _tableViewHeader.onlyAtWoolworthsLabel.text = [NSString stringWithFormat:@"$%.2f", woolworthsTotal];
    _tableViewHeader.totalSavingsLabel.text = [NSString stringWithFormat:@"$%.2f", savings];
}

#pragma mark - TableView Delegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _combinedArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray *)_combinedArray[section]).count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([_combinedArray objectAtIndex:section] == [DataSingleton sharedSingleton].colesShoppingList && [DataSingleton sharedSingleton].colesShoppingList.count) {
        CGFloat totalCost;
        
        for (PFObject *object in [DataSingleton sharedSingleton].colesShoppingList) {
            CGFloat colesPrice = [[[object objectForKey:@"colesprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
            totalCost += colesPrice;
        }
        return [NSString stringWithFormat:@"Coles: $%.2f", totalCost];
    }
    else if ([_combinedArray objectAtIndex:section] == [DataSingleton sharedSingleton].woolworthsShoppingList && [DataSingleton sharedSingleton].woolworthsShoppingList.count) {
        CGFloat totalCost;
        
        for (PFObject *object in [DataSingleton sharedSingleton].woolworthsShoppingList) {
            CGFloat woolworthsPrice = [[[object objectForKey:@"woolworthsprice"] stringByReplacingOccurrencesOfString:@"$" withString:@""] floatValue];
            totalCost += woolworthsPrice;
        }
        return [NSString stringWithFormat:@"Woolworths: $%.2f", totalCost];
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProductTableViewCell" owner:self options:kNilOptions] objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    cell.productName.text = [_combinedArray[indexPath.section][indexPath.row] objectForKey:@"productname"];
    cell.productSize.text = [[[_combinedArray[indexPath.section][indexPath.row] objectForKey:@"productsize"] stringByReplacingOccurrencesOfString:@"Units: " withString:@""] uppercaseString];
    
    cell.colesPrice.text = [_combinedArray[indexPath.section][indexPath.row] objectForKey:@"colesprice"];
    cell.woolworthsPrice.text = [_combinedArray[indexPath.section][indexPath.row] objectForKey:@"woolworthsprice"];
    
    return cell;
}

@end
