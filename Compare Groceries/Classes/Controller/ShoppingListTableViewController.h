//
//  ShoppingListTableViewController.h
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingListTableViewController : UITableViewController

@end
