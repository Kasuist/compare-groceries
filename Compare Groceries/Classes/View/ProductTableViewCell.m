//
//  ProductTableViewCell.m
//  Compare Groceries
//
//  Created by Beau Young on 3/12/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "ProductTableViewCell.h"

@implementation ProductTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.cardbackground.layer.cornerRadius = 5;
    self.cardbackground.clipsToBounds = YES;
    
    self.colesPrice.layer.cornerRadius = 3;
    self.colesPrice.clipsToBounds = YES;
    self.colesPrice.adjustsFontSizeToFitWidth = YES;
    
    self.woolworthsPrice.layer.cornerRadius = 3;
    self.woolworthsPrice.clipsToBounds = YES;
    self.woolworthsPrice.adjustsFontSizeToFitWidth = YES;
}

@end